
<?php
// Automation of  Python scripts generating at Lever ATS
//Nikolett Palyi HDCOMP,x15031144 , 30/11/2016

echo '<html><head><style>span{background-color: green;color:white;font-size;18px;}article{background-color: lightblue;}div{background-color: lightskyblue;}</style></head><body><form method="post"><input type="submit" name="script" value="Make Greenhouse Scripts"></form>';

$template = 'aproplan.py'; // the template which is to be used for generating Python scripts atLever ATS
$result = mysql_query("SELECT * FROM `recruiter` where recruiter_ats = 'lever' and CHAR_LENGTH('recruiter_career_page_url')>5 and script='No'"); // create a query in database, checks in Database, if the recruiter has scripts Bollean value 'No'.If 'No' the code will be execute on this recruiter and generate script

$skills = array(); // guess: in this array will be collected the generated scripts
$z=0;
while($row = mysql_fetch_assoc($result)) { // to fetch the result row as an associative array
    makeScript(strtolower(str_replace(' ', '-', $row["recruiter_company_name"])), $row["recruiter_id"], $row["recruiter_career_page_url"], $template); //Replace all occurrences of the search string with the replacement string, get the string to lower case
}

//makeScript("Aproplan",1498,"https://jobs.lever.co/aproplan",$template);


//should company name be lowercase
function copyTemplateScript($companyName, $recruiterId, $template){ //this function will duplicate the template 
    $newfile = $companyName.'_'.$recruiterId.'.py';

    if (!copy($template, $newfile)) { // copy function
        echo "<p>FAILED to copy-> $template\n</p>";
        echo "<p>FAILED to generate-> $newfile\n</p>";
        echoDashes(); 
        return false;
    }
    else{
        echo "<p>\tCopied-> $template\n</p>";
        echo "<p>\tGenerated-> $newfile\n</p>";
        echoDashes();
        return true;
    }

}

function makeScript($companyName, $recruiterId, $careerUrl, $template){  
    echo "<div>";
    $copy = copyTemplateScript($companyName, $recruiterId, $template); // this function is defined in line 18
    $file = $companyName.'_'.$recruiterId.'.py';
    $patternName = "/[nN][aA][mM][eE][=][\'\"][a-zA-Z]+[_][0-9]+[\'\"]/";
    $patternUrl = "/[uU][rR][lL][=][\'\"]https?:\/\/(.*)[\'\"]/";
    $name = $companyName . "_" . $recruiterId;
    $newName = "NAME = '$name'" . "\r\n";
    $newUrl = "URL = \"$careerUrl\"" . "\r\n";
    if($copy){ // defined in line 38
        $handle = doesFileExist($file); // $file=companyname_RID.py
        $linesArray = putLinesInArray($handle); // putlinesInArray defined in line 100
        if($linesArray !== null){
            for($i = 0; $i < count($linesArray); ++$i) {
                $trimmed = str_replace('\r\n', '', $linesArray[$i]); //Replace all occurrences of the search string with replacement string
                $check = str_replace(' ', '', $trimmed);//cleaned
                if (preg_match($patternName, $check)) { // to perform a regular expression match
                    // Updates name variable
                    echo "Old->\t". $linesArray[$i];
                    $linesArray[$i] = $newName;
                    echo "New->\t".$linesArray[$i];
                    echoDashes();
                    $nameIndex = $i;
                }
                elseif(preg_match($patternUrl, $check)){
                    // Updates url variable
                    echo "Old->\t". $linesArray[$i];
                    $linesArray[$i] = $newUrl;
                    echo "New->\t".$linesArray[$i];
                    echoDashes();
                    $urlIndex = $i;
                }
            }
            updateScriptVariables($newName, $newUrl, $file, $nameIndex, $urlIndex);
        }
        else{
            echo "<p>ABORTED array is null-> $file\n</p>";
            echoDashes();
        }
    }
    else{
        echo "<p>ABORTED generating the file-> $file\n</p>";
        echoDashes();
    }
}

function updateScriptVariables($newName, $newUrl, $my_file, $nameIndex, $urlIndex){  // this function will change the lines, values of the copy of template
    try{
        $lines = file( $my_file , FILE_IGNORE_NEW_LINES );
        $lines[$nameIndex] = $newName;
        $lines[$urlIndex] = $newUrl;
        file_put_contents( $my_file , implode( "\n", $lines ) ); //to write a string to the $ my_file and join $lines array elements with a string
        echoDashes();
        echo "\t<span>".$my_file."\t Created Successfully\n</span>";
        echoDashes();
        echo "</div>";
        file_put_contents("py_list_for_sh.txt", "python ".$my_file."\r", FILE_APPEND);
    }
    catch (Exception $e) { // exception handling
        echo 'Caught exception: ',  $e->getMessage(), "\n</p>";
        echo "\t".$my_file."\t FAILED to write";
    }
}

function putLinesInArray($handle){  // guess: exchange the strings in the copied file.
    if ($handle) {
        $z=0;
        $eachLineIsIndex=null;
        while (($line = fgets($handle)) !== false) {
            // process the line read.
            $eachLineIsIndex[$z]=$line;
            $z++;
        }
        fclose($handle); // to close the $handle
        return $eachLineIsIndex;
    } else {
        echo "ERROR opening the file";
        return null;
    }
}

function echoDashes(){
    echo "";
}

function doesFileExist($file){  // checks if the $file exists
    if(!file_exists($file)) {
        die("File not found");
    }else {
        $handle = fopen($file, "r"); 
        return $handle;
    }
}

echo "</body></html>";

?>